from django.urls import path

from .views import BackofficeSuggestionListView, SuggestionDeleteView

app_name = 'backoffice'
urlpatterns = [
    path('/suggestion/<int:pk>/delete/', SuggestionDeleteView.as_view(), name='suggestion_delete'),
    path('', BackofficeSuggestionListView.as_view(), name='home'),
]