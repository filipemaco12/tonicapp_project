from django.views.generic.edit import DeleteView
from django.views.generic import ListView, DetailView
from django.urls import reverse_lazy

from suggestion.models import Suggestion


class BackofficeSuggestionListView(ListView):
    queryset = Suggestion.objects.all()
    template_name = 'backoffice/home.html'
    context_object_name = 'suggestions'


class SuggestionDeleteView(DeleteView):
    model = Suggestion
    template_name = 'backoffice/suggestion_delete.html'
    success_url = reverse_lazy('suggestion:home')  # FIXME FILIPE ISTO PODE IR PARA A PAGINA GERAL DO BACKOFFICE




