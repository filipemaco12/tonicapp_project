from django.urls import path

from .views import SuggestionListView, SuggestionDetailView, SuggestionCreateView

app_name = 'suggestion'
urlpatterns = [
    path('suggestion/new/', SuggestionCreateView.as_view(), name='suggestion_new'),
    path('suggestion/<int:pk>/', SuggestionDetailView.as_view(), name='suggestion_detail'),
    path('', SuggestionListView.as_view(), name='home'),
]