from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView
from django.urls import reverse_lazy

from .models import Suggestion


class SuggestionListView(ListView):
    queryset = Suggestion.objects.all()
    template_name = 'suggestion/home.html'
    context_object_name = 'suggestions'


class SuggestionDetailView(DetailView):
    model = Suggestion
    template_name = 'suggestion/suggestion_detail.html'
    context_object_name = 'suggestion'


class SuggestionCreateView(CreateView):
    model = Suggestion
    template_name = 'suggestion/suggestion_new.html'
    fields = ['first_name', 'last_name', 'email', 'type', 'topic', 'body']



