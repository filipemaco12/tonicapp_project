from django.test import TestCase
from django.urls import reverse

from .models import Suggestion


class SuggestionTests(TestCase):

    def setUp(self):  # Create Suggestion
        self.suggestion = Suggestion.objects.create(
            first_name='João',
            last_name='Santos',
            email='joaosantos@example.com',
            type='REP',
            topic='Bug in header',
            body='No meu iPhone ao clicar no header dá um erro.',
        )

    def test_suggestion_content(self):
        self.assertEqual(f'{self.suggestion.topic}', 'Bug in header')
        self.assertEqual(f'{self.suggestion.REPORT}', 'REP')
        self.assertEqual(f'{self.suggestion.get_type_display()}', 'Report')
        self.assertEqual(f'{self.suggestion.body}', 'No meu iPhone ao clicar no header dá um erro.')
        self.assertEqual(self.suggestion.deleted, False)

    def test_suggestion_list_view(self):  # Check Template home.html
        response = self.client.get(reverse('suggestion:home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Bug in header')

    def test_suggestion_detail_view(self):  # Check Template suggestion_detail.html
        response = self.client.get('/suggestion/1/')
        self.assertTemplateUsed(response, 'suggestion/suggestion_detail.html')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'joaosantos@example.com')

        no_response = self.client.get('/suggestion/5/')
        self.assertEqual(no_response.status_code, 404)

    # FIXME PAREI NOS TESTES DO CREATE VIEW
    def test_suggestion_create_view(self):  # new
        response = self.client.post(reverse('suggestion:suggestion_new'), {
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'email': 'email@example.com',
            'type': 'SUG',
            'topic': 'Topic',
            'body': 'Body',
        })
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'First Name')
        self.assertContains(response, 'Last Name')
        self.assertContains(response, 'email@example.com')
        self.assertContains(response, 'Suggestion')
        self.assertContains(response, 'Topic')
        self.assertContains(response, 'Body')





