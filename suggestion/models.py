from django.db import models
from django.utils import timezone
from django.urls import reverse


class Suggestion(models.Model):
    SUGGESTION = 'SUG'
    REPORT = 'REP'
    OTHERS = 'OTH'

    TYPE_CHOICES = [
        (SUGGESTION, 'Suggestion'),
        (REPORT, 'Report'),
        (OTHERS, 'Others'),
    ]

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=80)

    type = models.CharField(max_length=3, choices=TYPE_CHOICES, default=OTHERS)
    topic = models.CharField(max_length=60)
    body = models.TextField(max_length=1000)

    pub_date = models.DateTimeField(default=timezone.now())
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.body[:40]

    def get_absolute_url(self):  # goes to suggestion_detail.html page
        return reverse('suggestion:suggestion_detail', args=[str(self.id)])

